# Typography - Tipografia

## É muito mais do que simplesmente escolher a fonte

  - É no trabalho de se escolher qual a melhor fonte que vai dividir, organizar e interpretar toda a massa de conteúdo de palavras e conteúdo, de uma maneira em que o leitor vai ter uma boa chance de encontrar o que é do interesse dele.
  - Um trabalho digital que não pode ser lido, se torna um produto **sem propósito**.

## Tipografia cria um impacto emocional

  - Como você ouve as frases de título a seguir?
  
  ![fontes1](images/fontes1.png)

  - Uma escolha apropriada da tipografia cria:
    - Legibilidade
    - Acessibilidade
    - Usabilidade
    - Balanço visual
  - Tudo isso trabalhando junto pra formar uma comunicação visual **efetiva** e **entendível**.

## Simplesmente escolher uma fonte não é tipografia
  - Qualquer um pode simplesmente escolher uma fonte
  - 