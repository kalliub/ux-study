# Usando cores e contraste apropriadamente
## Cores importam

  - Cores chamam a atenção, e para **evitar confusão** do significado, cores precisam ser utilizadas **consistentemente**.

    ![colorEx1](images/exampleCores1.png)

  - Cores instigam respostas emocionais e sugerem significado associado à elas. Para **conseguir a emoção desejada**, cores precisam ser utilizadas **apropriadamente**.

    ![colorEx2](images/exampleCores2.png)

  - Cores corretas **puxam o olho para as áreas mais importantes** na tela, podem **maximizar legibilidade** e **minimizar fadiga** e **entregam significados simbólicos**, que ajudam e melhoram a experiência visual do usuário.

    ![colorEx3](images/exampleCores3.png)

  - Cores agradam os olhos e **sustentam interesse visual**.

    ![colorEx4](images/exampleCores4.png)

## Como cores influenciam interação

  ![colorEx5](images/exampleCores5.png)

  `As cores azuis se relacionam demonstrando interação, orientando o usuário a executar a próxima ação`

  - Cores implicam **significado**! Ex: Verde para continuar, Amarelo para atenção, Vermelho para aviso intenso.
  - Cores saturadas ou brilhantes somente devem ser utilizadas como acentuações visuais.

## Usando cores CORRETAMENTE

  - Teoria de cores é útil, mas também é um saco. Você **nunca** vai entendê-la completamente, tampouco ter tempo pra aplicá-la durante um projeto.
  

  - Perguntas a se fazer:
    - Você está usando cores moderadamente?
    - As cores que escolheu reforçam ou interferem com a hierarquia e conteúdo? (Elas **guiam** o usuário ou **atrapalham** a orientação?)
    - O esquema de cores é usado **consistentemente**? (As cores escolhidas precisam ter o mesmo intuito do início ao fim da aplicação)
    - As cores são **funcionais** ou apenas decorativas? (Cores são escolhidas com uma **função**. Se você está escolhendo-as por achá-las legais, está escolhendo pelas razões erradas)
    - As funcionalidades *dependem* da cor? Se sim, **pessoas tem problemas enxergando cores**. Reveja isso! (Sugestão: também usar ícones para representar algo que é representado pela cor [como uma mensagem de erro, por exemplo].)

> Mantra: Use cores para **comunicar** e **influenciar** interação. Elas têm de fazer muito mais do que parecerem bonitinhas.

## Como escolher cores corretas para sua interface: 
### Associações comuns
  ![colorEx6](images/exampleCores6.png)

### Impacto Emocional

  - **Vermelho**:
    -  A cor vermelha tem um impacto fisiológico. Serve para aumentar a circulação sanguínea, respiração e metabolismo. Dependendo da combinação, é possível dar sentidos diferentes.
    -  O vermelho demanda ser notado, grita por atenção.
    -  Vermelho também pode significar **importância** e **prioridade**, como uma to-do list.

  ![coresVermelho1](images/coresVermelho1.png)
  ![coresVermelho2](images/coresVermelho2.png)
  ![coresVermelho3](images/coresVermelho3.png)
  ![coresVermelho4](images/coresVermelho4.png)

  - **Azul**:
    - O azul tende a ter efeitos de calma, e é uma cor convidativa devido às associações com o céu e a água. Pode também comunicar **calmaria** e **confiança** (bancos, tecnologia).
    - Um azul mais claro tende a sugerir algo **amigável**. Enquanto o mais escuro, sugere **confiança**.

  ![coresAzul1](images/coresAzul1.png)
  ![coresAzul2](images/coresAzul2.png)

  - **Verde**:
    - A cor verde normalmente é associada à **natureza**, **sucesso**, **crescimento** e **dinheiro**.
    - Normalmente é usado pra transparecer que está tudo bem, pra fazer com o que o usuário sinta calma.

  ![coresVerde1](images/coresVerde1.png)
  ![coresVerde2](images/coresVerde2.png)

  `Repare como o significado e sentimentos mudam de acordo com as cores.`

### Escolhendo do mundo em sua volta

  - Tudo que está em volta de você contém 4 variações de cores, requeridos para um bom design de interface:
    1. Shadows (Sombras): Normalmente as áreas mais escuras
    2. Midtones (Tons médios): Não tão claros, mas não tão escuros
    3. Highlights (Destaques): Uma área mais clara
    4. Accents (Acentuações): Normalmente uma anomalia em meio ao todo
  
  ***
  ![coresVariacao](images/coresVariacao.png)
  ***
  ![coresVariacao2](images/coresVariacao2.png)
  ***
  ![coresVariacao3](images/coresVariacao3.png)

### Escolhendo cores a partir de logos

  - A maioria das organizações tem um esquema de cores a partir da marca que vão querer que você comece (normalmente pelo menos uma logo).
  - Enquanto é um bom lugar para se começar, **cuidado** ao usar grandes áreas dessas cores na tela, pois **elas costumam ser muito saturadas**.
  - Cores vibrantes e saturadas causam tensão e fadiga ocular. Nos exemplos abaixo, as cores de acentuação que contiam na logo tiveram sua saturação reduzida, a fim de reduzir o possível cansaço ocular quando fosse usar numa interface.
  
  ***
  ![coresLogo1](images/coresLogo1.png)
  ***
  ![coresLogo2](images/coresLogo2.png)

> Mantra: Escolha as cores baseadas em **associações**, **emoções** e **marca**.

## O poder do contraste

  - Contraste é a relação entre o **claro** e o **escuro**.
  - O cérebro está ligado a sempre olhar para as diferenças no campo visual.

### Usando contraste para melhorar legibilidade, atenção e foco
  - Contraste direciona o foco. Em uma UI, **o local desejado para que o usuário direcione o foco, deve ressaltar em termos de contraste**.
  - Se o elemento não é a coisa mais importante na tela, você está direcionando o foco do usuário para o local errado.
  
  ![contraste1](images/contraste1.png)

  `Nesta tela, o que mais chama atenção é o quadrado colorido em meio ao fundo branco. Dentro dele, o que mais chama atenção e contrasta com tudo, é o botão na cor laranja, direcionando o interesse do usuário para prestar atenção nele primeiro, e então clicar.`

  - Um design com contraste **propositalmente** mantém o usuário focado onde ele precisa estar.
  - O conteúdo principal, ou ação, deve ter o maior contraste.

  ![contraste2](images/contraste2.png)

  `Nesta tela, a atenção vai diretamente pro que está escrito, e mais ainda para o campo de preenchimento, porque eles possuem uma relação de contraste massiva. A atenção que damos é consequência disso.`

  - Sendo assim, o contraste realiza 3 principais funções:
    1. Puxar a atenção do usuário para componentes essenciais para a interface.
    2. Ajuda o usuário a entender relações entre os elementos na tela.
    3. Comunicar a hierarquia e significar importância entre os multiplos lugares com informação visual.

  - Como exemplo, a tela do Gmail consegue mostrar que mesmo sem cor, o usuário ainda consegue se orientar do que é importante na tela.
  
    ![contraste3](images/contraste3.png)
    ![contraste4](images/contraste4.png)

    `Ainda é fácil detectar que o foco é o botão de escrever e o botão de pesquisa.`

  ***

  ![contraste5](images/contraste5.png)
  ![contraste6](images/contraste6.png)

> Mantra: Contraste **SEMPRE** ganha. Até mesmo de dominâncias de cor.