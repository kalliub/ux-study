# Organizando informação visual
## Balance - Balanço

  * Balancear o layout é organizar para que os elementos positivos e os espaços negativos não sobreponham um ao outro.
  * Tudo funciona e trabalha junto.
  
      ![balanceEx](/images/exampleBalance.png)
  
  * Ao invés de separar por elementos, é preciso orientar a visão por **grupos de conteúdo**.
    * Facilitar entendimento de grupos de objetos
    * Mínimo possível de elementos

> Mantra: Balanço cria ordem e relacionamento

## Rythm - Ritmo

  * Elementos com o mesmo ritmo relacionam-se entre eles, ajudando a compreensão do que representam
  
    ![rythmEx1](/images/exampleChatApp1.png)
    ![rythmEx2](/images/exampleChatApp2.png)

> Mantra: Ritmo visual acelera uso e compreensão

## Harmony - Harmonia

  * Quando elementos visuais estão em harmonia, eles se relacionam e se complementam.
  * Harmonia é uma grande parte do que agrupa elementos individuais em grupos maiores, para formar um "todo" mais coeso.
  * Repetição re-enfatiza elementos visuais, conectando e criando áreas de atenção e foco.

    ![harmonyEx1](/images/exampleHarmony.png)

    `Os elementos possuem o mesmo espaço negativo entre eles.`

  * Harmonia é um eco visual. As coisas que se parecem, **se repetem**
  * Harmonia também é usada pra criar um fluxo direcional de visão.
  * "Como as pessoas deverão ser guiadas pela página/tarefa?"

    ![harmonyEx2](images/exampleHarmony2.png)

    `Exemplo de Harmonia, onde os elementos se relacionam por suas alturas, larguras e espaços. Sempre buscando ecoar pela página.` 

> Mantra: Um bom design é mantido unido por uma boa **harmonia**

## Dominance - Dominância

  * Dominância -> Comando, controle, prevalência sobre tudo -> O fato de DOMINAR.
  
    ![dominanceEx1](images/exampleDominance1.png)

    `É impossível conseguir ficar sem olhar pro que domina toda a cena: a câmera! É o único ponto diferente (anomalia) em meio a um padrão de toda a tela.`

  * A dominância é alcançada enfatizando um ou mais elementos visuais. Cria um ponto de **foco**, **ativando e direcionando** o fluxo do usuário, onde a maioria das pessoas vão institivamente olhar de primeira. Daí então é desenvolvida uma **hierarquia** entre os elementos, ordenando qual vai ser visto _primeiro, segundo ou terceiro_.
  * A dominância depende do **contraste**. Limpa a diferença no campo visual.
  * Por exemplo, uma Modal: tudo ao redor é mais escuro, dando destaque e ênfase ao conteúdo que tá dentro da caixa.

    ![modalEx1](images/exampleModal.png)

  * Quando não há uma clara dominância entre os elementos, eles **competem** entre si. Sem um elemento dominante na tela, os usuários precisam se **esforçar** pra encontrar os pontos de foco por onde passar.
  * Estamos acostumados a **economizar esforço**, o que significa _o menor trabalho deve ser ir para outro design/outra tela/outro site/outro app_.

    ![dominanceEx2](images/exampleDominance2.png)

    `Como podemos transformar a área de foco mais dominante? Escurecendo todo o resto!`

  * Como NÃO alcançar a dominância:

    ![dominanceEx3](images/exemploDominance3.png)

> Mantra: Dominância direciona o foco do usuário.

## Alignment - Alinhamento

  * Alinhamento é um dos mais importantes princípios do design. Mesmo se nada for feito EXCETO usar um alinhamento propício, seus designs serão infinitamente mais úteis, usáveis e compreensíveis. O que também os faz serem mais valiosos.

  * Não alinhado || Alinhado
  
    ![alignmentEx1](images/exampleAlignment1.png)

  * Exemplo de alinhamento vertical e horizontal de todos os elementos com todos os elementos.
  
   ![alignmentEx2](images/exampleAlignment2.png)

> Mantra: Alinhe **tudo** com **tudo**

## Proximity - Proximidade

  * A proximidade é usada pra sinalizar **relação** entre elementos. Estes, visualizados uns perto dos outros, são vistos como um único grupo, relacionados por **contexto de uso**.
  * Itens não-relacionados são visualmente separados, uns longe dos outros.
  * Proximidade as vezes até mesmo **supera** cor e contraste!

    ![proximityEx1](images/exampleProximity1.png)

  * Usando a proximidade para tornar a **cognição mais rápida**:

    ![proximityEx2](images/exampleProximity2.png)

    ![proximityEx3](images/exampleProximity3.png)

  * Usando a proximidade para **melhorar a navegação**:

    ![proximityEx4](images/exampleProximity4.png)

    ![proximityEx5](images/exampleProximity5.png)

> Mantra: Sempre agrupar e organizar conteúdos relacionados com proximidade.